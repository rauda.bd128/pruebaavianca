-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: dbpracticaavianca
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbempleados`
--

DROP TABLE IF EXISTS `tbempleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbempleados` (
  `ID_Empleado` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Apellido` varchar(100) DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `Dui` varchar(15) DEFAULT NULL,
  `Nit` varchar(30) DEFAULT NULL,
  `Isss` varchar(30) DEFAULT NULL,
  `Afp` varchar(30) DEFAULT NULL,
  `ID_Genero` int DEFAULT NULL,
  `ID_TipoContrato` int DEFAULT NULL,
  `ID_Cargo` int DEFAULT NULL,
  `Sueldo` float DEFAULT NULL,
  `DateNewRow` datetime DEFAULT CURRENT_TIMESTAMP,
  `ID_Usuario` int DEFAULT NULL,
  PRIMARY KEY (`ID_Empleado`),
  KEY `fk_tbusuarios_tbempleados` (`ID_Usuario`),
  KEY `fk_tbgenero_tbempleados` (`ID_Genero`),
  KEY `fk_tbtiposcontratos_tbempleados` (`ID_TipoContrato`),
  KEY `fk_tbcargos_tbempleados` (`ID_Cargo`),
  CONSTRAINT `fk_tbcargos_tbempleados` FOREIGN KEY (`ID_Cargo`) REFERENCES `tbcargos` (`ID_Cargo`),
  CONSTRAINT `fk_tbgenero_tbempleados` FOREIGN KEY (`ID_Genero`) REFERENCES `tbgeneros` (`ID_Genero`),
  CONSTRAINT `fk_tbtiposcontratos_tbempleados` FOREIGN KEY (`ID_TipoContrato`) REFERENCES `tbtiposcontratos` (`ID_TipoContrato`),
  CONSTRAINT `fk_tbusuarios_tbempleados` FOREIGN KEY (`ID_Usuario`) REFERENCES `tbusuarios` (`ID_Usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbempleados`
--

LOCK TABLES `tbempleados` WRITE;
/*!40000 ALTER TABLE `tbempleados` DISABLE KEYS */;
INSERT INTO `tbempleados` VALUES (1,'Gustavo','Rauda','2008-08-06','12345678-9','0614-050599-111-5','0614-050599-111-5','987654',1,1,1,1500.98,'2022-07-09 13:22:04',1),(2,'Andres','Anzora','2022-10-11','98765432-1','4567-886611-556-6','4567-886611-556-6','987321',1,2,2,800,'2022-07-09 15:53:12',1),(3,'Ricardo','Fernandez','1999-05-05','05873749-9','0614-060688-121-6','0614-060688-121-6','856321567',1,3,2,1000,'2022-07-11 17:05:00',1);
/*!40000 ALTER TABLE `tbempleados` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-11 17:23:32
