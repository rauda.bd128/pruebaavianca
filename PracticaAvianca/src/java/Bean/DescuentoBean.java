/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author bryan
 */
public class DescuentoBean {
    
    private int ID_Descuento;
    private String Descuento;
    private double Procentaje;
    private String DateNewRow;
    private UsuarioBean ID_Usuario;

    public DescuentoBean(int ID_Descuentos) {
        this.ID_Descuento = ID_Descuentos;
    }

    public int getID_Descuento() {
        return ID_Descuento;
    }

    public void setID_Descuento(int ID_Descuentos) {
        this.ID_Descuento = ID_Descuentos;
    }

    public String getDescuento() {
        return Descuento;
    }

    public void setDescuento(String Descuento) {
        this.Descuento = Descuento;
    }

    public double getProcentaje() {
        return Procentaje;
    }

    public void setProcentaje(double Procentaje) {
        this.Procentaje = Procentaje;
    }

    public String getDateNewRow() {
        return DateNewRow;
    }

    public void setDateNewRow(String DateNewRow) {
        this.DateNewRow = DateNewRow;
    }

    public UsuarioBean getID_Usuario() {
        return ID_Usuario;
    }

    public void setID_Usuario(UsuarioBean ID_Usuario) {
        this.ID_Usuario = ID_Usuario;
    }
    
}
