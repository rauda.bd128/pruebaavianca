/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.Date;

/**
 *
 * @author bryan
 */
public class EmpleadoBean {
    private int ID_Empleado;
    private String Nombre;
    private String Apellido;
    private String FechaNacimiento;
    private String Dui;
    private String Nit;
    private String Isss;
    private String Afp;
    private GeneroBean ID_Genero;
    private TipoContratoBean ID_TipoContrato;
    private CargoBean ID_Cargo;
    private float Sueldo;
    private String DateNewRow;
    private UsuarioBean ID_Usuario;

    public EmpleadoBean(int ID_Empleado) {
        this.ID_Empleado = ID_Empleado;
    }

    public int getID_Empleado() {
        return ID_Empleado;
    }

    public void setID_Empleado(int ID_Empleado) {
        this.ID_Empleado = ID_Empleado;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    public String getDui() {
        return Dui;
    }

    public void setDui(String Dui) {
        this.Dui = Dui;
    }

    public String getNit() {
        return Nit;
    }

    public void setNit(String Nit) {
        this.Nit = Nit;
    }

    public String getIsss() {
        return Isss;
    }

    public void setIsss(String Isss) {
        this.Isss = Isss;
    }

    public String getAfp() {
        return Afp;
    }

    public void setAfp(String Afp) {
        this.Afp = Afp;
    }

    public GeneroBean getID_Genero() {
        return ID_Genero;
    }

    public void setID_Genero(GeneroBean ID_Genero) {
        this.ID_Genero = ID_Genero;
    }

    public TipoContratoBean getID_TipoContrato() {
        return ID_TipoContrato;
    }

    public void setID_TipoContrato(TipoContratoBean ID_TipoContrato) {
        this.ID_TipoContrato = ID_TipoContrato;
    }

    public CargoBean getID_Cargo() {
        return ID_Cargo;
    }

    public void setID_Cargo(CargoBean ID_Cargo) {
        this.ID_Cargo = ID_Cargo;
    }

    public float getSueldo() {
        return Sueldo;
    }

    public void setSueldo(float Sueldo) {
        this.Sueldo = Sueldo;
    }

    public String getDateNewRow() {
        return DateNewRow;
    }

    public void setDateNewRow(String DateNewRow) {
        this.DateNewRow = DateNewRow;
    }

    public UsuarioBean getID_Usuario() {
        return ID_Usuario;
    }

    public void setID_Usuario(UsuarioBean ID_Usuario) {
        this.ID_Usuario = ID_Usuario;
    }
    
}
