/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author bryan
 */
public class DetalleCalculoPlanillaBean {
    
    private String Dui;
    private String Descuento;
    private Float Porcentaje;
    private Float Total;

    public DetalleCalculoPlanillaBean(String Dui) {
        this.Dui = Dui;
    }

    public String getDui() {
        return Dui;
    }

    public void setDui(String Dui) {
        this.Dui = Dui;
    }

    public String getDescuento() {
        return Descuento;
    }

    public void setDescuento(String Descuento) {
        this.Descuento = Descuento;
    }

    public Float getPorcentaje() {
        return Porcentaje;
    }

    public void setPorcentaje(Float Porcentaje) {
        this.Porcentaje = Porcentaje;
    }

    public Float getTotal() {
        return Total;
    }

    public void setTotal(Float Total) {
        this.Total = Total;
    }
    
}
