/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author bryan
 */
public class DetalleTipoContratoBean {
    
    private int ID_DetalleTipoContrato;
    private TipoContratoBean ID_TipoContrato;
    private DescuentoBean ID_Descuento;

    public DetalleTipoContratoBean(int ID_DetalleTipoContrato) {
        this.ID_DetalleTipoContrato = ID_DetalleTipoContrato;
    }

    public int getID_DetalleTipoContrato() {
        return ID_DetalleTipoContrato;
    }

    public void setID_DetalleTipoContrato(int ID_DetalleTipoContrato) {
        this.ID_DetalleTipoContrato = ID_DetalleTipoContrato;
    }

    public TipoContratoBean getID_TipoContrato() {
        return ID_TipoContrato;
    }

    public void setID_TipoContrato(TipoContratoBean ID_TipoContrato) {
        this.ID_TipoContrato = ID_TipoContrato;
    }

    public DescuentoBean getID_Descuento() {
        return ID_Descuento;
    }

    public void setID_Descuento(DescuentoBean ID_Descuento) {
        this.ID_Descuento = ID_Descuento;
    }
    
}
