/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author bryan
 */
public class CargoBean {
    private int ID_Cargo;
    private String Cargo;
    private String DateNewRow;
    private UsuarioBean ID_Usuario;

    public CargoBean(int ID_Cargo) {
        this.ID_Cargo = ID_Cargo;
    }

    public int getID_Cargo() {
        return ID_Cargo;
    }

    public void setID_Cargo(int ID_Cargo) {
        this.ID_Cargo = ID_Cargo;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public String getDateNewRow() {
        return DateNewRow;
    }

    public void setDateNewRow(String DateNewRow) {
        this.DateNewRow = DateNewRow;
    }

    public UsuarioBean getID_Usuario() {
        return ID_Usuario;
    }

    public void setID_Usuario(UsuarioBean ID_Usuario) {
        this.ID_Usuario = ID_Usuario;
    }
    
}
