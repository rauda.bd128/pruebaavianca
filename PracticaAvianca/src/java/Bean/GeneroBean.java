/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author bryan
 */
public class GeneroBean {
    private int ID_Genero;
    private String Genero;

    public GeneroBean(int ID_Genero) {
        this.ID_Genero = ID_Genero;
    }

    public int getID_Genero() {
        return ID_Genero;
    }

    public void setID_Genero(int ID_Genero) {
        this.ID_Genero = ID_Genero;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }
    
}
