/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.List;

/**
 *
 * @author bryan
 */
public class CalculoPlanillaBean {
    
    private String Nombre;
    private String Apellido;
    private String Dui;
    private String TipoContrato;
    private String Cargo;
    private Float Sueldo;
    private List<DetalleCalculoPlanillaBean> Descuentos;
    private Float TotalDescuentos;
    private Float LiquidoRecibir;

    public CalculoPlanillaBean(String Dui) {
        this.Dui = Dui;
    }

    public Float getTotalDescuentos() {
        return TotalDescuentos;
    }

    public void setTotalDescuentos(Float TotalDescuentos) {
        this.TotalDescuentos = TotalDescuentos;
    }

    public Float getLiquidoRecibir() {
        return LiquidoRecibir;
    }

    public void setLiquidoRecibir(Float LiquidoRecibir) {
        this.LiquidoRecibir = LiquidoRecibir;
    }

    public List<DetalleCalculoPlanillaBean> getDescuentos() {
        return Descuentos;
    }

    public void setDescuentos(List<DetalleCalculoPlanillaBean> Descuentos) {
        this.Descuentos = Descuentos;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getDui() {
        return Dui;
    }

    public void setDui(String Dui) {
        this.Dui = Dui;
    }

    public String getTipoContrato() {
        return TipoContrato;
    }

    public void setTipoContrato(String TipoContrato) {
        this.TipoContrato = TipoContrato;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public Float getSueldo() {
        return Sueldo;
    }

    public void setSueldo(Float Sueldo) {
        this.Sueldo = Sueldo;
    }
    
}
