/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author bryan
 */
public class TipoContratoBean {
    
    private int ID_TipoContrato;
    private String TipoContrato;
    private String DateNewRow;
    private UsuarioBean ID_Usuario;

    public TipoContratoBean(int ID_TipoContrato) {
        this.ID_TipoContrato = ID_TipoContrato;
    }

    public int getID_TipoContrato() {
        return ID_TipoContrato;
    }

    public void setID_TipoContrato(int ID_TipoContrato) {
        this.ID_TipoContrato = ID_TipoContrato;
    }

    public String getTipoContrato() {
        return TipoContrato;
    }

    public void setTipoContrato(String TipoContrato) {
        this.TipoContrato = TipoContrato;
    }

    public String getDateNewRow() {
        return DateNewRow;
    }

    public void setDateNewRow(String DateNewRow) {
        this.DateNewRow = DateNewRow;
    }

    public UsuarioBean getID_Usuario() {
        return ID_Usuario;
    }

    public void setID_Usuario(UsuarioBean ID_Usuario) {
        this.ID_Usuario = ID_Usuario;
    }
    
}
