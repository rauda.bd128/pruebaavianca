/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.CargoBean;
import Bean.DescuentoBean;
import Bean.DetalleTipoContratoBean;
import Bean.EmpleadoBean;
import Bean.GeneroBean;
import Bean.TipoContratoBean;
import Conexion.conexion;
import Dao.CargoDao;
import Dao.DescuentoDao;
import Dao.DetalleTipoContratoDao;
import Dao.EmpleadoDao;
import Dao.GeneroDao;
import Dao.TipoContratoDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author bryan
 */
public class DashboardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    RequestDispatcher rd;
    conexion conn = new conexion();
    EmpleadoDao queryEmpleado = new EmpleadoDao(conn);
    List<EmpleadoBean> ListEmpleado = new LinkedList<EmpleadoBean>();
    GeneroDao queryGenero = new GeneroDao(conn);
    List<GeneroBean> ListGenero = new LinkedList<GeneroBean>();
    TipoContratoDao queryTipoContrato = new TipoContratoDao(conn);
    List<TipoContratoBean> ListTipoContrato = new LinkedList<TipoContratoBean>();
    CargoDao queryCargo = new CargoDao(conn);
    List<CargoBean> ListCargo = new LinkedList<CargoBean>();
    DetalleTipoContratoDao queryDetalleTipoContrato = new DetalleTipoContratoDao(conn);
    List<DetalleTipoContratoBean> ListDetalleTipoContrato = new LinkedList<DetalleTipoContratoBean>();
    DescuentoDao queryDescuento = new DescuentoDao(conn);
    List<DescuentoBean> ListDescuento = new LinkedList<DescuentoBean>();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getPathInfo();
        path = (path == null) ?"/" :path;
       
        switch(path){
            case "/Access":
                System.out.println("Nueva vista");
                break;
            case "/eliminar":
                
                break;
            default:
                
                HttpSession sessionUser = request.getSession();
                
                ListEmpleado = queryEmpleado.select();
                ListDescuento = queryDescuento.select();
                ListTipoContrato = queryTipoContrato.select();
                
                request.setAttribute("ListEmpleado", ListEmpleado);
                request.setAttribute("ListDescuento", ListDescuento);
                request.setAttribute("ListTipoContrato", ListTipoContrato);
                
                rd = request.getRequestDispatcher("/View/DashboardView.jsp");
                rd.forward(request, response);
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
