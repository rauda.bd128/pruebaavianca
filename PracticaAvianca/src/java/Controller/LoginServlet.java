/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.UsuarioBean;
import Conexion.conexion;
import Dao.UsuarioDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author bryan
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    RequestDispatcher rd;
    conexion conn = new conexion();
    UsuarioDao queryUsuario = new UsuarioDao(conn);
    List<UsuarioBean> ListUsuario = new LinkedList<UsuarioBean>();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String path = request.getPathInfo();
        path = (path == null) ?"/" :path;
        
        switch(path){
            case "/Validate":
                
                try {
                    
                    String email = request.getParameter("login-user");
                    String password = request.getParameter("login-password");
                    
                    UsuarioBean u = queryUsuario.select_validar(email, password);
                    HttpSession sessionUser = request.getSession();
                    
                    if (u.getID_Usuario() != 0) {
                
                        sessionUser.setAttribute("ID_Usuario", u.getID_Usuario());
                        sessionUser.setAttribute("Nombre", u.getNombre());
                        sessionUser.setAttribute("Apellido", u.getApellido());
                        sessionUser.setAttribute("IdTipoUsuario", u.getTipoUsuario());
                        switch(u.getTipoUsuario()){
                            case 0:
                                sessionUser.setAttribute("TipoUsuario", "Administrador General");
                                break;
                            case 1:
                                sessionUser.setAttribute("TipoUsuario", "Pagador de Planilla");
                                break;
                            case 2:
                                sessionUser.setAttribute("TipoUsuario", "Visualizacion");
                                break;
                        }
                        
                        response.sendRedirect("/PracticaAvianca/Dashboard/*");
                        
                    }else{
                        
                        response.sendRedirect("/PracticaAvianca/Login/*");
                    }
                    
                } catch (Exception e) {
                }
                
                break;
            case "/Destroy":
                request.getSession().invalidate();
                response.sendRedirect("/PracticaAvianca/Login/*");
                break;
            default:
                rd = request.getRequestDispatcher("/View/LoginView.jsp");
                rd.forward(request, response);
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
