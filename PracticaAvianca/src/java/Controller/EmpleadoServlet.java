/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.CalculoPlanillaBean;
import Bean.CargoBean;
import Bean.DescuentoBean;
import Bean.DetalleCalculoPlanillaBean;
import Bean.DetalleTipoContratoBean;
import Bean.EmpleadoBean;
import Bean.GeneroBean;
import Bean.TipoContratoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import Dao.CargoDao;
import Dao.DescuentoDao;
import Dao.DetalleTipoContratoDao;
import Dao.EmpleadoDao;
import Dao.GeneroDao;
import Dao.TipoContratoDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author bryan
 */
public class EmpleadoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    RequestDispatcher rd;
    conexion conn = new conexion();
    EmpleadoDao queryEmpleado = new EmpleadoDao(conn);
    List<EmpleadoBean> ListEmpleado = new LinkedList<EmpleadoBean>();
    GeneroDao queryGenero = new GeneroDao(conn);
    List<GeneroBean> ListGenero = new LinkedList<GeneroBean>();
    TipoContratoDao queryTipoContrato = new TipoContratoDao(conn);
    List<TipoContratoBean> ListTipoContrato = new LinkedList<TipoContratoBean>();
    CargoDao queryCargo = new CargoDao(conn);
    List<CargoBean> ListCargo = new LinkedList<CargoBean>();
    DetalleTipoContratoDao queryDetalleTipoContrato = new DetalleTipoContratoDao(conn);
    List<DetalleTipoContratoBean> ListDetalleTipoContrato = new LinkedList<DetalleTipoContratoBean>();
    DescuentoDao queryDescuento = new DescuentoDao(conn);
    List<DescuentoBean> ListDescuento = new LinkedList<DescuentoBean>();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String path = request.getPathInfo();
        path = (path == null) ?"/" :path;
        
        switch(path){
            case "/Insert":
        
                try {
                    HttpSession sessionUser = request.getSession();
                    
                    int ID_Empleado = Integer.parseInt(request.getParameter("txtID_Empleado"));
                    String Nombre = request.getParameter("txtNombre");
                    String Apellido = request.getParameter("txtApellido");
                    String FechaNacimiento = request.getParameter("txtFechaNacimiento");
                    String Dui = request.getParameter("txtDui");
                    String Nit = request.getParameter("txtNit");
                    String Isss = request.getParameter("txtIsss");
                    String Afp = request.getParameter("txtAfp");
                    int ID_Genero = Integer.parseInt(request.getParameter("slcID_Genero"));
                    int ID_TipoContrato = Integer.parseInt(request.getParameter("slcID_TipoContrato"));
                    int ID_Cargo = Integer.parseInt(request.getParameter("slcID_Cargo"));
                    float Sueldo = Float.parseFloat(request.getParameter("txtSueldo"));
                    
                    EmpleadoBean g = new EmpleadoBean(ID_Empleado);
                    GeneroBean Genero = new GeneroBean(ID_Genero);
                    TipoContratoBean tp = new TipoContratoBean(ID_TipoContrato);
                    CargoBean c = new CargoBean(ID_Cargo);
                    UsuarioBean u = new UsuarioBean(Integer.parseInt(sessionUser.getAttribute("ID_Usuario").toString()));
                    
                    g.setNombre(Nombre);
                    g.setApellido(Apellido);
                    g.setFechaNacimiento(FechaNacimiento);
                    g.setDui(Dui);
                    g.setNit(Nit);
                    g.setIsss(Isss);
                    g.setAfp(Afp);
                    g.setSueldo(Sueldo);
                    g.setID_Genero(Genero);
                    g.setID_TipoContrato(tp);
                    g.setID_Cargo(c);
                    g.setID_Usuario(u);
                    
                    if (ID_Empleado != 0) {
                        queryEmpleado.update(g);
                    }else{
                        queryEmpleado.insert(g);
                    }
                    
                    response.sendRedirect("/PracticaAvianca/Empleado/*");
                    
                } catch (Exception e) {
                    
                    System.out.println("Error Servlet: " + e.getMessage());
                    
                }
                
                break;
            case "/Planilla":
                
                try {
                    
                    ListEmpleado = queryEmpleado.select();
                    ListDetalleTipoContrato = queryDetalleTipoContrato.select();
                    ListDescuento = queryDescuento.select();
                    
                    List<CalculoPlanillaBean> ListaPlanilla = new LinkedList<CalculoPlanillaBean>();
                    
                    for(EmpleadoBean emp: ListEmpleado){
                        
                        List<DetalleCalculoPlanillaBean> ListDetalleCalculoPlanilla = new LinkedList<DetalleCalculoPlanillaBean>();
                        CalculoPlanillaBean cp = new CalculoPlanillaBean(emp.getDui());
                        Float Liquido = emp.getSueldo();
                        Double Descuentos = 0.0;
                        
                        cp.setNombre(emp.getNombre());
                        cp.setApellido(emp.getApellido());
                        cp.setTipoContrato(emp.getID_TipoContrato().getTipoContrato());
                        cp.setSueldo(emp.getSueldo());
                        cp.setCargo(emp.getID_Cargo().getCargo());
                        
                        List<DetalleTipoContratoBean> ListTempDetalleTipoContrato = new LinkedList<DetalleTipoContratoBean>();
                        
                        for(DescuentoBean des_temp: ListDescuento){
                            
                            ListTempDetalleTipoContrato = queryDetalleTipoContrato.select_validar(emp.getID_TipoContrato().getID_TipoContrato(), des_temp.getID_Descuento());
                            
                            DetalleCalculoPlanillaBean dcp = new DetalleCalculoPlanillaBean(emp.getDui());
                            
                            if (ListTempDetalleTipoContrato.size() != 0) {
                                
                                Float Descuento_detalle = ((emp.getSueldo() * Float.parseFloat(Double.toString(des_temp.getProcentaje()))) / 100);
                                Descuentos = Descuentos + Descuento_detalle;
                                dcp.setDescuento(des_temp.getDescuento());
                                dcp.setTotal(Descuento_detalle);
                                
                            }else{
                                dcp.setDescuento(des_temp.getDescuento());
                                dcp.setTotal(Float.parseFloat("0"));
                            }
                            
                            ListDetalleCalculoPlanilla.add(dcp);
                            
                        }
                        
                        cp.setTotalDescuentos(Float.parseFloat(Double.toHexString(Descuentos)));
                        cp.setLiquidoRecibir((Liquido - Float.parseFloat(Double.toHexString(Descuentos))));
                        
                        cp.setDescuentos(ListDetalleCalculoPlanilla);
                        
                        ListaPlanilla.add(cp);
                        
                    }
                    
                    request.setAttribute("ListaPlanilla", ListaPlanilla);
                    request.setAttribute("ListDetalleTipoContrato", ListDescuento);
                
                    rd = request.getRequestDispatcher("/View/PlanillaView.jsp");
                    rd.forward(request, response);
                    
                } catch (Exception e) {
                    
                    System.out.println("Error Servlet: " + e.getMessage());
                    
                }
                
                break;
            default:
                
                ListEmpleado = queryEmpleado.select();
                ListGenero = queryGenero.select();
                ListTipoContrato = queryTipoContrato.select();
                ListCargo = queryCargo.select();
                
                request.setAttribute("Lista", ListEmpleado);
                request.setAttribute("ListGenero", ListGenero);
                request.setAttribute("ListTipoContrato", ListTipoContrato);
                request.setAttribute("ListCargo", ListCargo);
                
                rd = request.getRequestDispatcher("/View/EmpleadosView.jsp");
                rd.forward(request, response);
                break;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
