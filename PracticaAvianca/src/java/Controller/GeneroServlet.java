/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.GeneroBean;
import Bean.GeneroBean;
import Conexion.conexion;
import Dao.GeneroDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author bryan
 */
public class GeneroServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    RequestDispatcher rd;
    conexion conn = new conexion();
    GeneroDao queryGenero = new GeneroDao(conn);
    List<GeneroBean> ListGenero = new LinkedList<GeneroBean>();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String path = request.getPathInfo();
        path = (path == null) ?"/" :path;
        
        switch(path){
            case "/Insert":
        
                try {
                    int ID_Genero = Integer.parseInt(request.getParameter("txtIdGenero"));
                    String Genero = request.getParameter("txtGenero");
                    
                    GeneroBean g = new GeneroBean(ID_Genero);
                    
                    g.setGenero(Genero);
                    
                    if (ID_Genero != 0) {
                        queryGenero.update(g);
                    }else{
                        queryGenero.insert(g);
                    }
                    
                    response.sendRedirect("/PracticaAvianca/Genero/*");
                    
                } catch (Exception e) {
                }
                
                break;
            case "/eliminar":
                
                break;
            default:
                
                ListGenero = queryGenero.select();
                
                request.setAttribute("Lista", ListGenero);
                
                rd = request.getRequestDispatcher("/View/GenerosView.jsp");
                rd.forward(request, response);
                break;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
