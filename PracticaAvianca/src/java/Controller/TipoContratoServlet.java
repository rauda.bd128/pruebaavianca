/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.DescuentoBean;
import Bean.TipoContratoBean;
import Bean.DetalleTipoContratoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import Dao.DescuentoDao;
import Dao.DetalleTipoContratoDao;
import Dao.TipoContratoDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author bryan
 */
public class TipoContratoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    RequestDispatcher rd;
    conexion conn = new conexion();
    TipoContratoDao queryTipoContrato = new TipoContratoDao(conn);
    List<TipoContratoBean> ListTipoContrato = new LinkedList<TipoContratoBean>();
    DetalleTipoContratoDao queryDetalleTipoContrato = new DetalleTipoContratoDao(conn);
    List<DetalleTipoContratoBean> ListDetalleTipoContrato = new LinkedList<DetalleTipoContratoBean>();
    DescuentoDao queryDescuento = new DescuentoDao(conn);
    List<DescuentoBean> ListDescuento = new LinkedList<DescuentoBean>();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getPathInfo();
        path = (path == null) ?"/" :path;
        
        switch(path){
            case "/InsertTipoContrato":
        
                try {
                    HttpSession sessionUser = request.getSession();
                    int ID_TipoContrato = Integer.parseInt(request.getParameter("txtIdTipoContrato"));
                    String TipoContrato = request.getParameter("txtTipoContrato");
                    
                    TipoContratoBean g = new TipoContratoBean(ID_TipoContrato);
                    UsuarioBean u = new UsuarioBean(Integer.parseInt(sessionUser.getAttribute("ID_Usuario").toString()));
                    
                    g.setTipoContrato(TipoContrato);
                    g.setID_Usuario(u);
                    
                    if (ID_TipoContrato != 0) {
                        queryTipoContrato.update(g);
                    }else{
                        queryTipoContrato.insert(g);
                    }
                    
                    response.sendRedirect("/PracticaAvianca/TipoContrato/*");
                    
                } catch (Exception e) {
                }
                
                break;
            case "/InsertDetalleTipoContrato":
        
                try {
                    int ID_DetalleTipoContrato = Integer.parseInt(request.getParameter("txtIdDetalleTipoContrato"));
                    int TipoContrato = Integer.parseInt(request.getParameter("slcTipoContrato"));
                    String[] Descuentos = request.getParameterValues("slcDescuento[]");
                    
                    for(String descuento: Descuentos){
                        
                        DetalleTipoContratoBean g = new DetalleTipoContratoBean(ID_DetalleTipoContrato);
                        TipoContratoBean tp = new TipoContratoBean(TipoContrato);
                        DescuentoBean d = new DescuentoBean(Integer.parseInt(descuento));


                        g.setID_TipoContrato(tp);
                        g.setID_Descuento(d);

                        if (ID_DetalleTipoContrato != 0) {
                            queryDetalleTipoContrato.update(g);
                        }else{
                            queryDetalleTipoContrato.insert(g);
                        }
                        
                    }
                    
                    response.sendRedirect("/PracticaAvianca/TipoContrato/*");
                    
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }
                
                break;
            case "/eliminar":
                
                break;
            default:
                
                ListTipoContrato = queryTipoContrato.select();
                ListDescuento = queryDescuento.select();
                ListDetalleTipoContrato = queryDetalleTipoContrato.select();
                
                request.setAttribute("ListaContratos", ListTipoContrato);
                request.setAttribute("ListaDescuentos", ListDescuento);
                request.setAttribute("ListDetalleTipoContrato", ListDetalleTipoContrato);
                
                rd = request.getRequestDispatcher("/View/TipoContratosView.jsp");
                rd.forward(request, response);
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
