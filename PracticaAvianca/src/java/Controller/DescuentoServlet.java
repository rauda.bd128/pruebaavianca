/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.DescuentoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import Dao.DescuentoDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author bryan
 */
public class DescuentoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    RequestDispatcher rd;
    conexion conn = new conexion();
    DescuentoDao queryDescuento = new DescuentoDao(conn);
    List<DescuentoBean> ListDescuento = new LinkedList<DescuentoBean>();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getPathInfo();
        path = (path == null) ?"/" :path;
        
        switch(path){
            case "/Insert":
        
                try {
                    
                    HttpSession sessionUser = request.getSession();
                    
                    int ID_Descuento = Integer.parseInt(request.getParameter("txtIdDescuento"));
                    String Descuento = request.getParameter("txtDescuento");
                    double Porcentaje = Double.parseDouble(request.getParameter("txtPorcentaje"));
                    
                    DescuentoBean g = new DescuentoBean(ID_Descuento);
                    UsuarioBean u = new UsuarioBean(Integer.parseInt(sessionUser.getAttribute("ID_Usuario").toString()));
                    
                    g.setDescuento(Descuento);
                    g.setProcentaje(Porcentaje);
                    g.setID_Usuario(u);
                    
                    if (ID_Descuento != 0) {
                        queryDescuento.update(g);
                    }else{
                        queryDescuento.insert(g);
                    }
                    
                    response.sendRedirect("/PracticaAvianca/Descuento/*");
                    
                } catch (Exception e) {
                }
                
                break;
            case "/eliminar":
                
                break;
            default:
                
                ListDescuento = queryDescuento.select();
                
                request.setAttribute("Lista", ListDescuento);
                
                rd = request.getRequestDispatcher("/View/DescuentosView.jsp");
                rd.forward(request, response);
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
