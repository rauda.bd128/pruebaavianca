/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Bean.TipoContratoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bryan
 */
public class TipoContratoDao {
    private conexion conn = new conexion();
    
    public TipoContratoDao(conexion conn){
        this.conn = conn;
    }
    
    public void insert(TipoContratoBean de){
        
        String sql = "INSERT INTO tbTiposContratos(ID_TipoContrato, TipoContrato, ID_Usuario) VALUES (?, ?, ?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_TipoContrato());
            ps.setString(2, de.getTipoContrato());
            ps.setInt(3, de.getID_Usuario().getID_Usuario());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void update(TipoContratoBean de){
        
        String sql = "UPDATE tbTiposContratos SET TipoContrato = ? WHERE ID_TipoContrato = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, de.getTipoContrato());
            ps.setInt(2, de.getID_TipoContrato());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void delete(TipoContratoBean de){
        
        String sql = "DELETE FROM tbTiposContratos WHERE ID_TipoContrato = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_TipoContrato());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public List<TipoContratoBean> select(){
        
        List<TipoContratoBean> list = new LinkedList<TipoContratoBean>();
        TipoContratoBean de = null;
        String sql = "SELECT * FROM tbTiposContratos INNER JOIN tbusuarios ON tbTiposContratos.ID_Usuario = tbusuarios.ID_Usuario";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de = new TipoContratoBean(rs.getInt(1));
                UsuarioBean u = new UsuarioBean(rs.getInt(5));
                
                u.setNombre(rs.getString(6));
                u.setApellido(rs.getString(7));
                u.setUsuario(rs.getString(8));
                u.setPass(rs.getString(9));
                u.setTipoUsuario(rs.getInt(10));
                
                de.setTipoContrato(rs.getString(2));
                de.setDateNewRow(rs.getString(3));
                de.setID_Usuario(u);
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
}
