/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Bean.UsuarioBean;
import Conexion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bryan
 */
public class UsuarioDao {
    private conexion conn = new conexion();
    
    public UsuarioDao(conexion conn){
        this.conn = conn;
    }
    
    public void insert(UsuarioBean de){
        
        String sql = "INSERT INTO tbusuarios VALUES (?, ?, ?, ?, md5(?), ?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Usuario());
            ps.setString(2, de.getNombre());
            ps.setString(3, de.getApellido());
            ps.setString(4, de.getUsuario());
            ps.setString(5, de.getPass());
            ps.setInt(6, de.getTipoUsuario());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void update(UsuarioBean de){
        
        String sql = "UPDATE tbusuarios SET Nombre = ?, Apellido = ?, Usuario = ?, Pass = md5(?), TipoUsuario = ? WHERE ID_Usuario = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, de.getNombre());
            ps.setString(2, de.getApellido());
            ps.setString(3, de.getUsuario());
            ps.setString(4, de.getPass());
            ps.setInt(5, de.getTipoUsuario());
            ps.setInt(6, de.getID_Usuario());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void delete(UsuarioBean de){
        
        String sql = "DELETE FROM tbusuarios WHERE ID_Usuario = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Usuario());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public List<UsuarioBean> select(){
        
        List<UsuarioBean> list = new LinkedList<UsuarioBean>();
        UsuarioBean de = null;
        String sql = "SELECT * FROM tbusuarios";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de = new UsuarioBean(rs.getInt(1));
                de.setNombre(rs.getString(2));
                de.setApellido(rs.getString(3));
                de.setUsuario(rs.getString(4));
                de.setPass(rs.getString(5));
                de.setTipoUsuario(rs.getInt(6));
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
    
    public UsuarioBean select_validar(String user, String contra){
        
        UsuarioBean de = new UsuarioBean(0);
        String sql = "SELECT * FROM tbusuarios WHERE Usuario = ? AND Pass = md5(?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, contra);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de.setID_Usuario(rs.getInt(1));
                de.setNombre(rs.getString(2));
                de.setApellido(rs.getString(3));
                de.setUsuario(rs.getString(4));
                de.setPass(rs.getString(5));
                de.setTipoUsuario(rs.getInt(6));
                
            }
            
            return de;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
}
