/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Bean.GeneroBean;
import Conexion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bryan
 */
public class GeneroDao {
    private conexion conn = new conexion();
    
    public GeneroDao(conexion conn){
        this.conn = conn;
    }
    
    public void insert(GeneroBean de){
        
        String sql = "INSERT INTO tbGeneros VALUES (?, ?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Genero());
            ps.setString(2, de.getGenero());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void update(GeneroBean de){
        
        String sql = "UPDATE tbGeneros SET Genero = ? WHERE ID_Genero = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, de.getGenero());
            ps.setInt(2, de.getID_Genero());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void delete(GeneroBean de){
        
        String sql = "DELETE FROM tbGeneros WHERE ID_Genero = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Genero());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public List<GeneroBean> select(){
        
        List<GeneroBean> list = new LinkedList<GeneroBean>();
        GeneroBean de = null;
        String sql = "SELECT * FROM tbGeneros";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de = new GeneroBean(rs.getInt(1));
                de.setGenero(rs.getString(2));
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
}
