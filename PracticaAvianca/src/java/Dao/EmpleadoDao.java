/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Bean.CargoBean;
import Bean.EmpleadoBean;
import Bean.GeneroBean;
import Bean.TipoContratoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bryan
 */
public class EmpleadoDao {
    private conexion conn = new conexion();
    
    public EmpleadoDao(conexion conn){
        this.conn = conn;
    }
    
    public void insert(EmpleadoBean de){
        
        String sql = "INSERT INTO tbEmpleados(ID_Empleado, Nombre, Apellido, FechaNacimiento, Dui, Nit, Isss, Afp, ID_Genero, ID_TipoContrato, ID_Cargo, Sueldo, ID_Usuario) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Empleado());
            ps.setString(2, de.getNombre());
            ps.setString(3, de.getApellido());
            ps.setString(4, de.getFechaNacimiento());
            ps.setString(5, de.getDui());
            ps.setString(6, de.getNit());
            ps.setString(7, de.getNit());
            ps.setString(8, de.getAfp());
            ps.setInt(9, de.getID_Genero().getID_Genero());
            ps.setInt(10, de.getID_TipoContrato().getID_TipoContrato());
            ps.setInt(11, de.getID_Cargo().getID_Cargo());
            ps.setDouble(12, de.getSueldo());
            ps.setInt(13, de.getID_Usuario().getID_Usuario());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void update(EmpleadoBean de){
        
        String sql = "UPDATE tbEmpleados SET Nombre = ?, Apellido = ?, FechaNacimiento = ?, Dui = ?, Nit = ?, Isss = ?, Afp = ?, ID_Genero = ?, ID_TipoContrato = ?, ID_Cargo = ?, Sueldo = ?, ID_Usuario = ? WHERE ID_Empleado = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, de.getNombre());
            ps.setString(2, de.getApellido());
            ps.setString(3, de.getFechaNacimiento());
            ps.setString(4, de.getDui());
            ps.setString(5, de.getNit());
            ps.setString(6, de.getIsss());
            ps.setString(7, de.getAfp());
            ps.setInt(8, de.getID_Genero().getID_Genero());
            ps.setInt(9, de.getID_TipoContrato().getID_TipoContrato());
            ps.setInt(10, de.getID_Cargo().getID_Cargo());
            ps.setDouble(11, de.getSueldo());
            ps.setInt(12, de.getID_Usuario().getID_Usuario());
            ps.setInt(13, de.getID_Empleado());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void delete(EmpleadoBean de){
        
        String sql = "DELETE FROM tbEmpleados WHERE ID_Empleado = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Empleado());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public List<EmpleadoBean> select(){
        
        List<EmpleadoBean> list = new LinkedList<EmpleadoBean>();
        EmpleadoBean de = null;
        String sql = "SELECT * FROM tbEmpleados INNER JOIN tbusuarios ON tbusuarios.ID_Usuario = tbempleados.ID_Usuario INNER JOIN tbgeneros ON tbgeneros.ID_Genero = tbempleados.ID_Genero INNER JOIN tbtiposcontratos ON tbtiposcontratos.ID_TipoContrato = tbempleados.ID_TipoContrato INNER JOIN tbcargos ON tbcargos.ID_Cargo = tbempleados.ID_Cargo";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de = new EmpleadoBean(rs.getInt(1));
                UsuarioBean u = new UsuarioBean(rs.getInt(15));
                GeneroBean g = new GeneroBean(rs.getInt(21));
                TipoContratoBean tp = new TipoContratoBean(rs.getInt(23));
                CargoBean c = new CargoBean(rs.getInt(27));
                
                c.setCargo(rs.getString(28));
                
                tp.setTipoContrato(rs.getString(24));
                
                g.setGenero(rs.getString(22));
                
                u.setNombre(rs.getString(16));
                u.setApellido(rs.getString(17));
                u.setUsuario(rs.getString(18));
                u.setPass(rs.getString(19));
                u.setTipoUsuario(rs.getInt(20));
                
                de.setNombre(rs.getString(2));
                de.setApellido(rs.getString(3));
                de.setFechaNacimiento(rs.getString(4));
                de.setDui(rs.getString(5));
                de.setNit(rs.getString(6));
                de.setIsss(rs.getString(7));
                de.setAfp(rs.getString(8));
                de.setID_Genero(g);
                de.setID_TipoContrato(tp);
                de.setID_Cargo(c);
                de.setSueldo(rs.getFloat(12));
                de.setDateNewRow(rs.getString(13));
                de.setID_Usuario(u);
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
}
