/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Bean.DescuentoBean;
import Bean.DetalleTipoContratoBean;
import Bean.DetalleTipoContratoBean;
import Bean.TipoContratoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bryan
 */
public class DetalleTipoContratoDao {
    private conexion conn = new conexion();
    
    public DetalleTipoContratoDao(conexion conn){
        this.conn = conn;
    }
    
    public void insert(DetalleTipoContratoBean de){
        
        String sql = "INSERT INTO tbdetallestiposcontratos VALUES (?, ?, ?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_DetalleTipoContrato());
            ps.setInt(2, de.getID_TipoContrato().getID_TipoContrato());
            ps.setInt(3, de.getID_Descuento().getID_Descuento());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void update(DetalleTipoContratoBean de){
        
        String sql = "UPDATE tbdetallestiposcontratos SET ID_TipoContrato = ?, ID_Descuento = ? WHERE ID_DetalleTipoContrato = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_TipoContrato().getID_TipoContrato());
            ps.setInt(2, de.getID_Descuento().getID_Descuento());
            ps.setInt(3, de.getID_DetalleTipoContrato());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void delete(DetalleTipoContratoBean de){
        
        String sql = "DELETE FROM tbdetallestiposcontratos WHERE ID_DetalleTipoContrato = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_DetalleTipoContrato());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public List<DetalleTipoContratoBean> select(){
        
        List<DetalleTipoContratoBean> list = new LinkedList<DetalleTipoContratoBean>();
        DetalleTipoContratoBean de = null;
        String sql = "SELECT * FROM tbdetallestiposcontratos INNER JOIN tbtiposcontratos ON tbdetallestiposcontratos.ID_TipoContrato = tbtiposcontratos.ID_TipoContrato INNER JOIN tbdescuentos ON tbdetallestiposcontratos.ID_Descuento = tbdescuentos.ID_Descuento";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de = new DetalleTipoContratoBean(rs.getInt(1));
                TipoContratoBean tp = new TipoContratoBean(rs.getInt(2));
                DescuentoBean d = new DescuentoBean(rs.getInt(3));
                
                tp.setTipoContrato(rs.getString(5));
                
                d.setDescuento(rs.getString(9));
                d.setProcentaje(rs.getDouble(10));
                
                de.setID_TipoContrato(tp);
                de.setID_Descuento(d);
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
    
    public List<DetalleTipoContratoBean> select_validar(Integer ID_TipoContrato, Integer ID_Descuento){
        
        List<DetalleTipoContratoBean> list = new LinkedList<DetalleTipoContratoBean>();
        DetalleTipoContratoBean de = new DetalleTipoContratoBean(0);
        String sql = "SELECT * FROM tbdetallestiposcontratos WHERE ID_TipoContrato = ? AND ID_Descuento = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, ID_TipoContrato);
            ps.setInt(2, ID_Descuento);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                TipoContratoBean tp = new TipoContratoBean(rs.getInt(2));
                DescuentoBean d = new DescuentoBean(rs.getInt(3));
                
                de.setID_DetalleTipoContrato(rs.getInt(1));
                de.setID_Descuento(d);
                de.setID_TipoContrato(tp);
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
}
