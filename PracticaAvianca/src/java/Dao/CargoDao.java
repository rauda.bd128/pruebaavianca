/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Bean.CargoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bryan
 */
public class CargoDao {
    private conexion conn = new conexion();
    
    public CargoDao(conexion conn){
        this.conn = conn;
    }
    
    public void insert(CargoBean de){
        
        String sql = "INSERT INTO tbcargos (ID_Cargo, Cargo, ID_Usuario) VALUES(?, ?, ?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Cargo());
            ps.setString(2, de.getCargo());
            ps.setInt(3, de.getID_Usuario().getID_Usuario());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void update(CargoBean de){
        
        String sql = "UPDATE tbcargos SET Cargo = ? WHERE ID_Cargo = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, de.getCargo());
            ps.setInt(2, de.getID_Cargo());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void delete(CargoBean de){
        
        String sql = "DELETE FROM tbcargos WHERE ID_Cargo = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Cargo());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public List<CargoBean> select(){
        
        List<CargoBean> list = new LinkedList<CargoBean>();
        CargoBean de = null;
        String sql = "SELECT * FROM tbcargos INNER JOIN tbusuarios ON tbcargos.ID_Usuario = tbusuarios.ID_Usuario";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de = new CargoBean(rs.getInt(1));
                UsuarioBean u = new UsuarioBean(rs.getInt(5));
                
                u.setNombre(rs.getString(6));
                u.setApellido(rs.getString(7));
                u.setUsuario(rs.getString(8));
                u.setPass(rs.getString(9));
                u.setTipoUsuario(rs.getInt(10));
                
                de.setCargo(rs.getString(2));
                de.setDateNewRow(rs.getString(3));
                de.setID_Usuario(u);
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
}
