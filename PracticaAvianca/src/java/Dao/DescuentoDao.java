/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Bean.DescuentoBean;
import Bean.UsuarioBean;
import Conexion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author bryan
 */
public class DescuentoDao {
    private conexion conn = new conexion();
    
    public DescuentoDao(conexion conn){
        this.conn = conn;
    }
    
    public void insert(DescuentoBean de){
        
        String sql = "INSERT INTO tbDescuentos(ID_Descuento, Descuento, Procentaje, ID_Usuario) VALUES (?, ?, ?, ?)";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Descuento());
            ps.setString(2, de.getDescuento());
            ps.setDouble(3, de.getProcentaje());
            ps.setInt(4, de.getID_Usuario().getID_Usuario());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void update(DescuentoBean de){
        
        String sql = "UPDATE tbDescuentos SET Descuento = ?, Procentaje = ? WHERE ID_Descuento = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, de.getDescuento());
            ps.setDouble(2, de.getProcentaje());
            ps.setInt(3, de.getID_Descuento());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public void delete(DescuentoBean de){
        
        String sql = "DELETE FROM tbDescuentos WHERE ID_Descuento = ?";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, de.getID_Descuento());
            ps.executeUpdate();
            
        } catch (Exception e) {
            
            System.out.println("Error: " + e.getMessage());
            
        }
        
    }
    
    public List<DescuentoBean> select(){
        
        List<DescuentoBean> list = new LinkedList<DescuentoBean>();
        DescuentoBean de = null;
        String sql = "SELECT * FROM tbDescuentos INNER JOIN tbusuarios ON tbdescuentos.ID_Usuario = tbusuarios.ID_Usuario";
        
        try {
            
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                
                de = new DescuentoBean(rs.getInt(1));
                UsuarioBean u = new UsuarioBean(rs.getInt(6));
                
                u.setNombre(rs.getString(7));
                u.setApellido(rs.getString(8));
                u.setUsuario(rs.getString(9));
                u.setPass(rs.getString(10));
                u.setTipoUsuario(rs.getInt(11));
                
                de.setDescuento(rs.getString(2));
                de.setProcentaje(rs.getDouble(3));
                de.setDateNewRow(rs.getString(4));
                de.setID_Usuario(u);
                
                list.add(de);
                
            }
            
            return list;
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        
    }
    
}
