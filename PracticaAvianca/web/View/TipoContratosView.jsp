<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../Layout/Header.jsp" %>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tipo de Contrato</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-2">
            Aqui puedes ingresar los tipos de contratos que necesites para los tipos de empleados
          </p>
          
          <form method="POST" action="/PracticaAvianca/TipoContrato/InsertTipoContrato">
              <input type="hidden" id="txtIdTipoContrato" name="txtIdTipoContrato" value="0"/>
              <div class="row">
                <div class="col-12 mb-1">
                    <div class="form-group">
                        <label for="txtTipoContrato">Tipo de Contrato</label>
                        <input type="text" class="form-control" id="txtTipoContrato" name="txtTipoContrato">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Descuentos</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-2">
            Aqui puedes ingresar los descuentos para luego agregarlos a los tipos de contratos
          </p>
          
          <form method="POST" action="/PracticaAvianca/Descuento/Insert">
              <input type="hidden" id="txtIdDescuento" name="txtIdDescuento" value="0"/>
            <div class="row">
                <div class="col-6 mb-1">
                    <div class="form-group">
                        <label for="txtDescuento">Descuento</label>
                        <input type="text" class="form-control" id="txtDescuento" name="txtDescuento">
                    </div>
                </div>
                <div class="col-6 mb-1">
                    <div class="form-group">
                        <label for="txtPorcentaje">Procentaje</label>
                        <input type="text" class="form-control" id="txtPorcentaje" name="txtPorcentaje">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Contratos</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-2">
            Aqui puedes Seleccionar un tipo de contrato y agregarlos los descuentos que necesites
          </p>
          
          <form method="POST" action="/PracticaAvianca/TipoContrato/InsertDetalleTipoContrato">
              <input type="hidden" id="txtIdDetalleTipoContrato" name="txtIdDetalleTipoContrato" value="0"/>
            <div class="row">
                <div class="col-6 mb-1">
                    <div class="form-group">
                        <label for="slcTipoContrato">Tipo de Contrato</label>
                        <select id="slcTipoContrato" name="slcTipoContrato" class="form-select select2">
                            <option value="null">-- Seleccione --</option>
                            <c:forEach items="${ListaContratos}" var="item">
                                <option value="${item.ID_TipoContrato}">${item.tipoContrato}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-6 mb-1">
                    <div class="form-group">
                        <label for="slcDescuento">Descuentos</label>
                        <select id="slcDescuento" name="slcDescuento[]" class="form-select select2" multiple>
                            <option value="null">-- Seleccione --</option>
                            <c:forEach items="${ListaDescuentos}" var="item">
                                <option value="${item.ID_Descuento}">${item.descuento} - ${item.procentaje}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table nowrap" id="tbinfo">
                            <thead>
                                <tr>
                                    <th>Contrato</th>
                                    <th>Descuento</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${ListDetalleTipoContrato}" var="item">
                                    <tr>
                                        <td>${item.ID_TipoContrato.tipoContrato}</td>
                                        <td>${item.ID_Descuento.descuento} (${item.ID_Descuento.procentaje})</td>
                                        <td>
                                            <button class="btn btn-warning" type="button" onclick="Cargar()">Editar</button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="../Layout/Script.jsp" %>

<script>
    $("#tbinfo").DataTable();
    
    function Cargar(iD_Usuario, nombre, apellido, usuario, pass, tipoUsuario){
        $("#txtIdUsuario").val(iD_Usuario);
        $("#txtNombre").val(nombre);
        $("#txtApellido").val(apellido);
        $("#txtUsuario").val(usuario);
        $("#txtPass").val(pass);
        $("#slcTipoUsuario").val(tipoUsuario);
    }
</script>
