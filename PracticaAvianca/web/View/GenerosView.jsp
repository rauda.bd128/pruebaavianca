<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../Layout/Header.jsp" %>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
          <form method="POST" action="/PracticaAvianca/Genero/Insert">
              <input type="hidden" id="txtIdGenero" name="txtIdGenero" value="0"/>
              <div class="row">
                <div class="col-12 mb-1">
                    <div class="form-group">
                        <label for="txtGenero">Genero</label>
                        <input type="text" class="form-control" id="txtGenero" name="txtGenero">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table nowrap" id="tbinfo">
                            <thead>
                                <tr>
                                    <th>Genero</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${Lista}" var="item">
                                    <tr>
                                        <td>${item.genero}</td>
                                        <td>
                                            <button class="btn btn-warning" type="button" onclick="Cargar('${item.ID_Genero}', '${item.genero}')">Editar</button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="../Layout/Script.jsp" %>

<script>
    $("#tbinfo").DataTable();
    
    function Cargar(iD_Usuario, nombre){
        $("#txtIdGenero").val(iD_Usuario);
        $("#txtGenero").val(nombre);
    }
</script>
