<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../Layout/Header.jsp" %>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
          <form method="POST" action="/PracticaAvianca/Empleado/Insert">
            <input type="hidden" id="txtID_Empleado" name="txtID_Empleado" value="0"/>
            <div class="row">
                <div class="col-4 mb-1">
                    <div class="form-group">
                        <label for="txtNombre">Nombre</label>
                        <input type="text" class="form-control" id="txtNombre" name="txtNombre">
                    </div>
                </div>
                <div class="col-4 mb-1">
                    <div class="form-group">
                        <label for="txtApellido">Apellido</label>
                        <input type="text" class="form-control" id="txtApellido" name="txtApellido">
                    </div>
                </div>
                <div class="col-4 mb-1">
                    <div class="form-group">
                        <label for="txtFechaNacimiento">Fecha de Nacimiento</label>
                        <input type="text" class="form-control" id="txtFechaNacimiento" name="txtFechaNacimiento">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="txtDui">DUI</label>
                        <input type="text" class="form-control" id="txtDui" name="txtDui">
                    </div>
                </div>
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="txtNit">NIT</label>
                        <input type="text" class="form-control" id="txtNit" name="txtNit">
                    </div>
                </div>
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="txtIsss">ISSS</label>
                        <input type="text" class="form-control" id="txtIsss" name="txtIsss">
                    </div>
                </div>
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="txtAfp">AFP</label>
                        <input type="text" class="form-control" id="txtAfp" name="txtAfp">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="slcID_Genero">Genero</label>
                        <select id="slcID_Genero" name="slcID_Genero" class="form-select select2">
                            <option value="null">-- Seleccione --</option>
                            <c:forEach items="${ListGenero}" var="item">
                                <option value="${item.ID_Genero}">${item.genero}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="slcID_TipoContrato">Tipo de Contrato</label>
                        <select id="slcID_TipoContrato" name="slcID_TipoContrato" class="form-select select2">
                            <option value="null">-- Seleccione --</option>
                            <c:forEach items="${ListTipoContrato}" var="item">
                                <option value="${item.ID_TipoContrato}">${item.tipoContrato}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="slcID_Cargo">Cargo</label>
                        <select id="slcID_Cargo" name="slcID_Cargo" class="form-select select2">
                            <option value="null">-- Seleccione --</option>
                            <c:forEach items="${ListCargo}" var="item">
                                <option value="${item.ID_Cargo}">${item.cargo}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-3 mb-1">
                    <div class="form-group">
                        <label for="txtSueldo">Sueldo</label>
                        <input type="text" class="form-control" id="txtSueldo" name="txtSueldo">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table nowrap" id="tbinfo">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Dui</th>
                                    <th>Nit</th>
                                    <th>Isss</th>
                                    <th>Afp</th>
                                    <th>Genero</th>
                                    <th>Tipo de Contrato</th>
                                    <th>Cargo</th>
                                    <th>Sueldo</th>
                                    <th>DateNewRow</th>
                                    <th>Usuario</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${Lista}" var="item">
                                    <tr>
                                        <td>${item.nombre}</td>
                                        <td>${item.apellido}</td>
                                        <td>${item.fechaNacimiento}</td>
                                        <td>${item.dui}</td>
                                        <td>${item.nit}</td>
                                        <td>${item.isss}</td>
                                        <td>${item.afp}</td>
                                        <td>${item.ID_Genero.genero}</td>
                                        <td>${item.ID_TipoContrato.tipoContrato}</td>
                                        <td>${item.ID_Cargo.cargo}</td>
                                        <td>${item.sueldo}</td>
                                        <td>${item.dateNewRow}</td>
                                        <td>${item.ID_Usuario.nombre} ${item.ID_Usuario.apellido}</td>
                                        <td>
                                            <button class="btn btn-warning" type="button" onclick="Cargar(${item.ID_Empleado}, ${item.nombre}, ${item.apellido}, ${item.fechaNacimiento}, ${item.dui}, ${item.nit}, ${item.isss}, ${item.afp}, ${item.ID_Genero.ID_Genero}, ${item.ID_TipoContrato.ID_TipoContrato}, ${item.ID_Cargo.ID_Cargo}, ${item.sueldo})">Editar</button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="../Layout/Script.jsp" %>

<script>
    $("#tbinfo").DataTable();
    
    function Cargar(ID_Empleado, nombre, apellido, fechaNacimiento, dui, nit, isss, afp, ID_Genero, ID_TipoContrato, ID_Cargo, sueldo){
        $("#txtID_Empleado").val(ID_Empleado);
        $("#txtNombre").val(nombre);
        $("#txtApellido").val(apellido);
        $("#txtFechaNacimiento").val(fechaNacimiento);
        $("#txtDui").val(dui);
        $("#txtNit").val(nit);
        $("#txtIsss").val(isss);
        $("#txtAfp").val(afp);
        $("#txtID_Genero").val(ID_Genero);
        $("#txtID_TipoContrato").val(ID_TipoContrato);
        $("#txtID_Cargo").val(ID_Cargo);
        $("#txtSueldo").val(sueldo);
    }
</script>
