<%@include file="../Layout/Header.jsp" %>

<section>
  <div class="row">
    <div class="col-4">
        <div class="card text-center">
        <div class="card-body">
          <div class="avatar bg-light-info p-50 mb-1">
            <div class="avatar-content">
              <i data-feather="users" class="font-medium-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder">${ListEmpleado.size()}</h2>
          <p class="card-text">Empleados en el sistema</p>
        </div>
      </div>
    </div>
      <div class="col-4">
          <div class="card text-center">
        <div class="card-body">
          <div class="avatar bg-light-info p-50 mb-1">
            <div class="avatar-content">
              <i data-feather="align-center" class="font-medium-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder">${ListDescuento.size()}</h2>
          <p class="card-text">Descuentos en el sistema</p>
        </div>
      </div>
    </div>
      <div class="col-4">
          <div class="card text-center">
        <div class="card-body">
          <div class="avatar bg-light-info p-50 mb-1">
            <div class="avatar-content">
              <i data-feather="paperclip" class="font-medium-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder">${ListTipoContrato.size()}</h2>
          <p class="card-text">Contratos en el sistema</p>
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="../Layout/Script.jsp" %>