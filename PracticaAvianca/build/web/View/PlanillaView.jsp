<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../Layout/Header.jsp" %>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Reporte de Planilla</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Aqui se puede ver el reporte de planilla para todos los empleados con sus descuentos segun contrato
          </p>
          
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table nowrap" id="tbinfo">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>DUI</th>
                                    <th>Tipo de Contrato</th>
                                    <th>Cargo</th>
                                    <th>Sueldo</th>
                                    <c:forEach items="${ListDetalleTipoContrato}" var="item">
                                        <th>${item.descuento}</th>
                                    </c:forEach>
                                    <th>Total Descuentos</th>
                                    <th>Liquido a recibir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${ListaPlanilla}" var="item">
                                    <tr>
                                        <td>${item.nombre}</td>
                                        <td>${item.apellido}</td>
                                        <td>${item.dui}</td>
                                        <td>${item.tipoContrato}</td>
                                        <td>${item.cargo}</td>
                                        <td>${item.sueldo}</td>
                                        <c:forEach items="${item.descuentos}" var="descuentos">
                                                <td>${descuentos.total}</td>
                                        </c:forEach>
                                        <td>${item.totalDescuentos}</td>
                                        <td>${item.liquidoRecibir}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="../Layout/Script.jsp" %>

<script>
    $("#tbinfo").DataTable();
</script>
