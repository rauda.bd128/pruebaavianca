<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../Layout/Header.jsp" %>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
          <form method="POST" action="/PracticaAvianca/Usuario/Insert">
              <input type="hidden" id="txtIdUsuario" name="txtIdUsuario" value="0"/>
              <div class="row">
                <div class="col-6 mb-1">
                    <div class="form-group">
                        <label for="txtNombre">Nombre</label>
                        <input type="text" class="form-control" id="txtNombre" name="txtNombre">
                    </div>
                </div>
                <div class="col-6 mb-1">
                    <div class="form-group">
                        <label for="txtApellido">Apellido</label>
                        <input type="text" class="form-control" id="txtApellido" name="txtApellido">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4 mb-1">
                    <div class="form-group">
                        <label for="txtUsuario">Usuario</label>
                        <input type="text" class="form-control" id="txtUsuario" name="txtUsuario">
                    </div>
                </div>
                <div class="col-4 mb-1">
                    <div class="form-group">
                        <label for="txtPass">Contraseņa</label>
                        <input type="text" class="form-control" id="txtPass" name="txtPass">
                    </div>
                </div>
                <div class="col-4 mb-1">
                    <div class="form-group">
                        <label for="slcTipoUsuario">Tipo de Usuario</label>
                        <select name="slcTipoUsuario" id="slcTipoUsuario" class="form-select">
                          <option value="0">Administrador General</option>
                          <option value="1">Pagador de Planilla</option>
                          <option value="2">Visualizacion</option>
                      </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table nowrap" id="tbinfo">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Usuario</th>
                                    <th>Contraseņa</th>
                                    <th>Tipo de Usuario</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${Lista}" var="item">
                                    <tr>
                                        <td>${item.nombre}</td>
                                        <td>${item.apellido}</td>
                                        <td>${item.usuario}</td>
                                        <td>${item.pass}</td>
                                        <c:choose>
                                            <c:when test="${item.tipoUsuario == 0}">
                                                <td>Administrador General</td>
                                            </c:when>
                                            <c:when test="${item.tipoUsuario == 1}">
                                                <td>Pagador de Planilla</td>
                                            </c:when>
                                            <c:when test="${item.tipoUsuario == 2}">
                                                <td>Visualizacion</td>
                                            </c:when>
                                        </c:choose>
                                        <td>
                                            <button class="btn btn-warning" type="button" onclick="Cargar('${item.ID_Usuario}', '${item.nombre}', '${item.apellido}', '${item.usuario}', '${item.pass}', '${item.tipoUsuario}')">Editar</button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="../Layout/Script.jsp" %>

<script>
    $("#tbinfo").DataTable();
    
    function Cargar(iD_Usuario, nombre, apellido, usuario, pass, tipoUsuario){
        $("#txtIdUsuario").val(iD_Usuario);
        $("#txtNombre").val(nombre);
        $("#txtApellido").val(apellido);
        $("#txtUsuario").val(usuario);
        $("#txtPass").val(pass);
        $("#slcTipoUsuario").val(tipoUsuario);
    }
</script>
