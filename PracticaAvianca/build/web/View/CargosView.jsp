<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../Layout/Header.jsp" %>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
          <form method="POST" action="/PracticaAvianca/Cargo/Insert">
              <input type="hidden" id="txtIdCargo" name="txtIdCargo" value="0"/>
              <div class="row">
                <div class="col-12 mb-1">
                    <div class="form-group">
                        <label for="txtCargo">Cargo</label>
                        <input type="text" class="form-control" id="txtCargo" name="txtCargo">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Filled</h4>
        </div>
        <div class="card-body">
          <p class="card-text mb-0">
            Bootstrap includes six predefined button styles, each serving its own semantic purpose.
          </p>
          
            <div class="row">
                <div class="col-12">
                    <div class="card-datatable">
                        <table class="table" id="tbinfo">
                            <thead>
                                <tr>
                                    <th>Cargo</th>
                                    <th>Fecha de Insercion</th>
                                    <th>Usuario</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${Lista}" var="item">
                                    <tr>
                                        <td>${item.cargo}</td>
                                        <td>${item.dateNewRow}</td>
                                        <td>${item.ID_Usuario.nombre} ${item.ID_Usuario.apellido}</td>
                                        <td>
                                            <button class="btn btn-warning" type="button" onclick="Cargar('${item.ID_Cargo}', '${item.cargo}')">Editar</button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="../Layout/Script.jsp" %>

<script>
    $("#tbinfo").DataTable();
    
    function Cargar(iD_Usuario, nombre){
        $("#txtIdCargo").val(iD_Usuario);
        $("#txtCargo").val(nombre);
    }
</script>
